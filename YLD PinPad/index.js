import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import pinPadApp from './scripts/reducers';
import App from './scripts/App';

const store = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)(pinPadApp);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
