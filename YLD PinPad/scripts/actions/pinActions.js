import security from '../api/security';

export const checkPin = (pin, enter) => {
  //Response codes
  //0 - OK
  //1 - Fail (auto)
  //2 - Fail (enter)

  return dispatch => {
    var response = security.checkPin(pin);

    dispatch(checkPinSuccess(response ? 0 : enter ? 2 : 1));
    //For when API returns promise - running sync for hardcoded value instead
    // return security.checkPin(pin).then(response => {
    //     dispatch(checkPinSuccess(response));
    // }).catch(error => {
    //     throw (error);
    // });
  };
};

export const checkPinSuccess = response => {
  return {
    type: 'CHECK_PIN_SUCCESS',
    response,
  };
};
