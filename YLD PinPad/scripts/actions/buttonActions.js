export const sendButtonCommand = command => {
  return dispatch => {
    return dispatch({
      type: 'BUTTON_COMMAND',
      command,
    });
  };
};
