import { connect } from 'react-redux';
import { sendButtonCommand, checkPin } from '../actions';
import PadButton from '../components/padButton';

const mapStateToProps = (state, ownProps) => {
  return {
    command: ownProps.command,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    clickFunc: command => {
      ownProps.clickFunc
        ? ownProps.clickFunc()
        : dispatch(sendButtonCommand(command));
    },
  };
};

let PinPadButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(PadButton);

export default PinPadButton;
