import PinPad from '../components/pinPad';

import { connect } from 'react-redux';
import { checkPin } from '../actions';

const mapStateToProps = state => {
  return {
    security: state.security,
    pin: state.pin,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkPin: (pin, enter) => {
      dispatch(checkPin(pin, enter));
    },
  };
};

let Pad = connect(
  mapStateToProps,
  mapDispatchToProps
)(PinPad);

export default Pad;
