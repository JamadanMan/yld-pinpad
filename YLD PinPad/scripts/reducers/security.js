const security = (state = 1, action) => {
    //Default value to fail (auto)
    switch (action.type) {
        case 'CHECK_PIN_SUCCESS':
            return action.response
        default:
            return state
    }
}

export default security;
