import { combineReducers } from 'redux'
import pin from './pin'
import security from './security'

const pinPadApp = combineReducers({
    pin,
    security
});

export default pinPadApp
