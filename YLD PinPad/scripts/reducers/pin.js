const pin = (state = '', action) => {
  switch (action.type) {
    case 'BUTTON_COMMAND':
      switch (action.command) {
        case 'Del':
          return state.length < 2 ? '' : state.substring(0, state.length - 1);
        case 'Enter':
          return state;
        default:
          return state.length === 8 ? state : state + action.command.toString();
      }
    default:
      return state;
  }
};

export default pin;
