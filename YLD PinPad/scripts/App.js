import React from 'react'
import Pad from './containers/pinPadContainer'
import styles from '../css/ripped.css'

const App = () => (
  <div>
    <Pad />    
  </div>
)

export default App
