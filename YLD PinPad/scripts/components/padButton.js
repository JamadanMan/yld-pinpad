import React from 'react';

let PadButton = ({ clickFunc, command }) => {
  return (
    <div className="pad-button" onClick={() => clickFunc(command)}>
      {command}
    </div>
  );
};

export default PadButton;
