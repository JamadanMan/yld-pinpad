import React from 'react';
import PadButton from '../containers/padButtonContainer';

class PinPad extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(newProps) {
    //If pin changes - auto check
    if (newProps.pin !== this.props.pin) {
      this.props.checkPin(newProps.pin);
    }
  }

  visiblePin() {
    const code = this.props.security;
    const pin = this.props.pin;
    return code === 0
      ? 'OK'
      : code === 2
        ? 'ERROR'
        : pin.substring(0, pin.length - 1).replace(/./g, '*') +
          pin.substring(pin.length - 1);
  }

  render() {
    return (
      <div className="pin-pad">
        <div className="logo">JAM PIN PAD</div>
        <hr />
        <input
          className="pad-display"
          type="text"
          name="pin"
          readOnly
          value={this.visiblePin()}
        />
        <PadButton command="1" />
        <PadButton command="2" />
        <PadButton command="3" />
        <PadButton command="4" />
        <PadButton command="5" />
        <PadButton command="6" />
        <PadButton command="7" />
        <PadButton command="8" />
        <PadButton command="9" />
        <PadButton command="Del" />
        <PadButton command="0" />
        <PadButton
          command="Enter"
          clickFunc={() => this.props.checkPin(this.props.pin, true)}
        />
      </div>
    );
  }
}

export default PinPad;
