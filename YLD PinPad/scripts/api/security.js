const goodPin = (pin) => {
    //API call can be made here

    //Hardcoding 12345
    return pin === '12345';
};

export default {
    checkPin(pin) {
        return (goodPin(pin));
    }
}
